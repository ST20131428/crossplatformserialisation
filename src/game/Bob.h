#include "OutputMemoryStream.h"
#include "InputMemoryStream.h"

class Bob {

private:

	//examples of data going over streams
	float i, j, k;
	int x, y, z;

public:

	Bob();
	Bob(float i, float j, float k, int x, int y, int z);
	~Bob();

	void setPosX(int x);
	void setPosY(int y);
	void setPosZ(int z);

	int getPosX();
	int getPosY();
	int getPosZ();

	void Write(OutputMemoryStream& bobOut);
	void Read(InputMemoryStream& bobIn);

};