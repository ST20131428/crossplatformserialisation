#include "OutputMemoryStream.h"
#include "InputMemoryStream.h"

class Fred {

private:

	//examples of data going over streams
	std::string name;
	std::vector<int> numbers;
	Vector3 position;
	Quaternion orientation;

public:

	Fred();
	Fred(std::string name, std::vector<int> numbers, Vector3 position, Quaternion orientation);
	~Fred();

	void setName(std::string name);
	std::string getName();



	void Write(OutputMemoryStream& fredOut);
	void Read(InputMemoryStream& fredIn);

};