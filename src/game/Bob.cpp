#include "Bob.h"

Bob::Bob() : i(0.0f), j(0.0f), k(0.0f), x(0), y(0), z(0) {

}

Bob::Bob(float i, float j, float k, int x, int y, int z) {

	this->i = i;
	this->j = j;
	this->k = k;

	this->x = x;
	this->y = y;
	this->z = z;

}

Bob::~Bob() {

	//cleanup stuff but i wasnt bothered
}

//set dem gets
void Bob::setPosX(int x) {

	this->x = x;
}

void Bob::setPosY(int y) {

	this->y = y;
}

void Bob::setPosZ(int z) {
	
	this->z = z;
}

int Bob::getPosX() {

	return this->x;
}

int Bob::getPosY() {

	return this->y;
}

int Bob::getPosZ() {

	return this->z;
}

void Bob::Write(OutputMemoryStream& bobOut) {

	bobOut.Write(x);
	bobOut.Write(y);
	bobOut.Write(z);
}


void Bob::Read(InputMemoryStream& bobIn) {

	bobIn.Read(x);
	bobIn.Read(y);
	bobIn.Read(z);
}

