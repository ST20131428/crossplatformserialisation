
#include "InputMemoryStream.h"
#include "InputMemoryBitStream.h"

#include "OutputMemoryStream.h"
#include "OutputMemoryBitStream.h"

#include "Bob.h"

#include <iostream>

#if _WIN32
#include <Windows.h>
#endif


#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

}

int main(void) {

	//linker>>system>>subsystem>>console

	OutputMemoryStream* outPutStrem = new OutputMemoryStream();
	
	Bob bob(0.0f, 0.0f, 0.0f, 1, 0, 0);

	bob.Write(*outPutStrem);

	char* inBuffer = new char[128];
	int inBufferLength = outPutStrem->GetLength();

	memcpy(inBuffer, outPutStrem->GetBufferPtr(), inBufferLength);

	InputMemoryStream *inPutStrem = new InputMemoryStream(inBuffer, inBufferLength);

	Bob otherBob = Bob();

	otherBob.Read(*inPutStrem);

	
}

#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
